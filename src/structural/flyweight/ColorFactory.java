package structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class ColorFactory {

    private static Map<ColorType,Color> availableColors = new HashMap<>();

    public static Color retrieveColor(ColorType colorType) {
        Color color = availableColors.get(colorType);
        if (color != null) return color;
        if (colorType == ColorType.BLACK) {
            color = new Color(255,255,255);
        } else if (colorType == ColorType.BLUE) {
            color = new Color(0,0,255);
        } else if (colorType == ColorType.RED) {
            color = new Color(255,0,0);
        } else if (colorType == ColorType.GREEN) {
            color = new Color(0,255,0);
        } else if (colorType == ColorType.WHITE){
            color = new Color(255,255,255);
        }
        return availableColors.put(colorType, color);
    }
}
