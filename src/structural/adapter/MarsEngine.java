package structural.adapter;

public interface MarsEngine {
    public void setDirecttion(Direction goal);
    public void driveForward(int sec);
    public void driveBackward(int sec);
}
