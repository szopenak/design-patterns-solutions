package structural.composite;

import java.math.BigDecimal;

public interface PurchaseAble {
    public BigDecimal getPrice();
}
