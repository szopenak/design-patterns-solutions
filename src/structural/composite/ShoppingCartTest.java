package structural.composite;

import static junit.framework.TestCase.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import structural.composite.containers.Box;
import structural.composite.products.Chocolate;
import structural.composite.products.Cola;
import structural.composite.products.Cookies;
import structural.composite.products.Snack;

public class ShoppingCartTest {
    @Test
    public void validateShoppingCartTotalPrice(){
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Snack());
        shoppingCart.addProduct(new Chocolate());
        shoppingCart.addProduct(new Chocolate());

        //when

        //then
        assertEquals(new BigDecimal("23"),shoppingCart.getTotalCost());
    }

    @Test
    public void validateShoppingCartTotalPriceIncludingBoxes(){
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Snack());

        Box box1 = new Box();
        box1.addProduct(new Chocolate());
        box1.addProduct(new Chocolate());

        Box box2 = new Box();
        box2.addProduct(new Cola());
        box2.addProduct(new Cola());
        box2.addProduct(new Cola());
        box2.addProduct(new Cola());

        shoppingCart.addProduct(box1);
        shoppingCart.addProduct(box2);

        //when

        //then
        assertEquals(new BigDecimal("27"),shoppingCart.getTotalCost());
    }
}
