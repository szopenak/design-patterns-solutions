package structural.composite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    List<PurchaseAble> shopping = new ArrayList<>();

    public void addProduct(PurchaseAble product){
        shopping.add(product);
    }

    public BigDecimal getTotalCost(){
        BigDecimal totalPrice = new BigDecimal("0");
        for (PurchaseAble p : this.shopping) {
            totalPrice = totalPrice.add(p.getPrice());
        }
        return totalPrice;
    }
}
