package structural.composite.containers;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Box implements PurchaseAble {
    private BigDecimal boxPrice = new BigDecimal("1");
    private List<PurchaseAble> productsInside = new ArrayList<>();
    public void addProduct(PurchaseAble product){
        productsInside.add(product);
    }

    @Override
    public BigDecimal getPrice() {
        BigDecimal totalPrice = boxPrice;
        for (PurchaseAble p : this.productsInside) {
            totalPrice = totalPrice.add(p.getPrice());
        }
        return totalPrice;
    }
}
