package creational.builder;

public class Hamburger implements Meal{
    private Bacon bacon;
    private Cheese cheese;
    private Lettuce lettuce;
    private Sauce sauce;
    private boolean salty;
    private boolean spicy;
    private boolean extraCheese;
    private BasicRoll roll;
    private Meat meat;

    public static class HamburgerBuilder {
        private Bacon bacon;
        private Cheese cheese;
        private Lettuce lettuce;
        private Sauce sauce;
        private boolean salty;
        private boolean spicy;
        private boolean extraCheese;
        private BasicRoll roll;
        private Meat meat;

        public HamburgerBuilder(BasicRoll roll, Meat meat) {
            this.roll = roll;
            this.meat = meat;
        }

        public HamburgerBuilder withBacon(Bacon bacon) {
            this.bacon = bacon;
            return this;
        }
        public HamburgerBuilder withCheese(Cheese cheese) {
            this.cheese = cheese;
            return this;
        }
        public HamburgerBuilder withSauce(Sauce sauce) {
            this.sauce = sauce;
            return this;
        }
        public HamburgerBuilder withLettuce(Lettuce lettuce) {
            this.lettuce = lettuce;
            return this;
        }
        public HamburgerBuilder withSalty(boolean salty) {
            this.salty = salty;
            return this;
        }
        public HamburgerBuilder withSpicy(boolean spicy) {
            this.spicy = spicy;
            return this;
        }
        public HamburgerBuilder withExtraCheese(boolean extraCheese) {
            this.extraCheese = extraCheese;
            return this;
        }
        public Hamburger build(){
            return new Hamburger(this);
        }
    }

    public Hamburger(HamburgerBuilder builder) {
        this.bacon = builder.bacon;
        this.cheese = builder.cheese;
        this.lettuce = builder.lettuce;
        this.sauce = builder.sauce;
        this.salty = builder.salty;
        this.spicy = builder.spicy;
        this.extraCheese = builder.extraCheese;
        this.roll = builder.roll;
        this.meat = builder.meat;
    }

    @Override
    public String toString() {
        return "Hamburger{" +
                "bacon=" + bacon +
                ", cheese=" + cheese +
                ", lettuce=" + lettuce +
                ", sauce=" + sauce +
                ", salty=" + salty +
                ", spicy=" + spicy +
                ", extraCheese=" + extraCheese +
                ", roll=" + roll +
                ", meat=" + meat +
                '}';
    }

    @Override
    public void eat() {
        System.out.println("Eat Hamburger!" + toString());
    }
}
