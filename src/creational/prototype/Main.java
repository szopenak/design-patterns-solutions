package creational.prototype;

import creational.prototype.side_meals.Fries;
import creational.prototype.side_meals.FryingOil;

public class Main {
    public static void main(String... args) {
        FryingOil bigOilPot = new FryingOil();
        bigOilPot.warmUp();
        for (int i = 0; i <10; i++) {
            FryingOil fryingOil = bigOilPot.splitOil();
            Fries fries = new Fries();
            fryingOil.makeFries(fries);
            consume(fries);
        }

    }

    private static void consume(Fries f) {
        System.out.println(f);
    }
}