package creational.prototype.side_meals;

public interface Prototype {
    public FryingOil splitOil();
}
