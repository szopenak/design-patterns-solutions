package creational.singleton;

import creational.builder.Meal;

public class InnerStaticFastFoodConsumer {
    private InnerStaticFastFoodConsumer() {
        System.out.println("Bill Pugh singleton instance created!");
    }

    public static InnerStaticFastFoodConsumer getInstance() {
        return InnerClassHelper.SINGLETON;
    }

    public void deliver(Meal meal) {
        meal.eat();
    }

    public static void someMethod(){
        System.out.println("Some method static call in Bill Pugh singleton!");
    }

    private static class InnerClassHelper {
        private static InnerStaticFastFoodConsumer SINGLETON = new InnerStaticFastFoodConsumer();
    }
}
