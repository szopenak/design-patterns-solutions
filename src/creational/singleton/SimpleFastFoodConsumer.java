package creational.singleton;

import creational.builder.Meal;

public class SimpleFastFoodConsumer {

    private SimpleFastFoodConsumer() {System.out.println("Eager Initialization singleton instance created!");}

    private static SimpleFastFoodConsumer single = new SimpleFastFoodConsumer();

    public static SimpleFastFoodConsumer getInstance() {
        return single;
    }

    public void deliver(Meal meal) {
        meal.eat();
    }

    public static void someMethod(){
        System.out.println("Some method static call in Eager Initialization singleton!");
    }
}
