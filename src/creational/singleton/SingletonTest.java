package creational.singleton;

import org.junit.Test;

public class SingletonTest
{
    @Test
    public void showDifferencesBetweenSingletons(){
        SimpleFastFoodConsumer eager;
        InnerStaticFastFoodConsumer lazy;
        System.out.println("After declaration!");

        SimpleFastFoodConsumer.someMethod();
        InnerStaticFastFoodConsumer.someMethod();
        System.out.println("After static method use!");

        eager = SimpleFastFoodConsumer.getInstance();
        lazy = InnerStaticFastFoodConsumer.getInstance();
        System.out.println("After get instance!");
    }
}
