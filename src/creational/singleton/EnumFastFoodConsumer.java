package creational.singleton;

import creational.builder.Meal;

public enum EnumFastFoodConsumer {
    INSTANCE;

    public void deliver(Meal meal) {
        meal.eat();
    }
}
